# Kaggle Movies Dataset

To spin up and Elasticsearch node and a Kibana instance, navigate to _demo/elasticsearch_ and run: ```docker-compose up -d```

Then you can run the tests in ```IngestMoviesMetadata``` to index the dataset.