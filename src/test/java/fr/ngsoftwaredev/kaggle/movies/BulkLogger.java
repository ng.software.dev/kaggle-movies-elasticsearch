package fr.ngsoftwaredev.kaggle.movies;

import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;

@Slf4j
public class BulkLogger implements BulkProcessor.Listener {

  @Override
  public void beforeBulk(long executionId, BulkRequest request) {}

  @Override
  public void afterBulk(long executionId, BulkRequest request, BulkResponse response) {
    if (response.hasFailures()) {
      log.warn(
          "Processed bulk of {} requests ({} failures)",
          request.numberOfActions(),
          Stream.of(response.getItems()).filter(BulkItemResponse::isFailed).count());
    } else {
      log.info(
          "Processed bulk of {} requests successfully",
          request.numberOfActions());
    }
  }

  @Override
  public void afterBulk(long executionId, BulkRequest request, Throwable failure) {
    log.error("Bulk failure", failure);
  }

}
