package fr.ngsoftwaredev.kaggle.movies;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.client.RequestOptions.DEFAULT;
import static org.testcontainers.shaded.org.apache.commons.lang3.ObjectUtils.isEmpty;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.http.HttpHost;
import org.apache.http.util.EntityUtils;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.refresh.RefreshRequest;
import org.elasticsearch.action.bulk.BackoffPolicy;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.Request;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

@Slf4j
public class IngestMoviesMetadata {

  private static final String INDEX_BASE_NAME = "kaggle_movies_metadata";
  private static final String CSV_RESOURCE = "movies_metadata.csv";

  private enum Header {
    adult,
    belongs_to_collection,
    budget,
    genres,
    homepage,
    id,
    imdb_id,
    original_language,
    original_title,
    overview,
    popularity,
    poster_path,
    production_companies,
    production_countries,
    release_date,
    revenue,
    runtime,
    spoken_languages,
    status,
    tagline,
    title,
    video,
    vote_average,
    vote_count
  }

  private static final ObjectMapper SUB_JSON_MAPPER =
      new ObjectMapper().configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
  private static RestHighLevelClient esClient;

  @BeforeAll
  public static void initElasticsearchClient() {
    HttpHost httpHost = HttpHost.create("http://localhost:9201");
    RestClientBuilder clientBuilder = RestClient.builder(httpHost);
    esClient = new RestHighLevelClient(clientBuilder);
  }

  @AfterAll
  public static void closeElasticsearchClient() throws IOException {
    esClient.close();
  }

  @Test
  public void step0_ingestWithAutoMapping() throws IOException {
    ingest(INDEX_BASE_NAME + "_auto", Optional.empty(), true);
  }

  @Test
  public void step1_ingestWithCustomMapping() throws IOException {
    ingest(
        INDEX_BASE_NAME + "_step1",
        Optional.of("elasticsearch/kaggle_movies_metadata_step1.json"),
        true);
  }

  @Test
  public void step2_ingestWithCustomMappingAnsSettings() throws IOException {
    ingest(
        INDEX_BASE_NAME + "_step2",
        Optional.of("elasticsearch/kaggle_movies_metadata_step2.json"),
        true);
  }

  private static String resource(String filePath) throws IOException {
    try (InputStream data =
        IngestMoviesMetadata.class.getClassLoader().getResourceAsStream(filePath)) {
      if (data == null) throw new RuntimeException("Found no resource " + filePath);
      return new String(data.readAllBytes());
    }
  }

  private static String cleanSubJson(String subJson) {
    return subJson.replaceAll(",\\s+'(?:\\w+)'\\:\\s+None", "").replaceAll("\\\\", "\\\\\\\\");
  }

  private void ingest(String index, Optional<String> optIndexDefinition, boolean deleteExisting)
      throws IOException {

    if (deleteExisting && esClient.indices().exists(new GetIndexRequest(index), DEFAULT)) {
      log.info("Will delete existing index '{}'", index);
      assertThat(esClient.indices().delete(new DeleteIndexRequest(index), DEFAULT).isAcknowledged())
          .isTrue();
    }

    if (optIndexDefinition.isPresent()) {
      String createIndexPayloadResource = optIndexDefinition.get();
      log.info(
          "Will create index '{}' with definition resource '{}'",
          index,
          createIndexPayloadResource);
      assertThat(
              esClient
                  .indices()
                  .create(
                      new CreateIndexRequest(index)
                          .source(resource(createIndexPayloadResource), XContentType.JSON),
                      DEFAULT)
                  .isAcknowledged())
          .isTrue();
    }

    long readCount = 0L;
    long skipCount = 0L;
    try (BulkProcessor bulkProcessor =
        BulkProcessor.builder(
                (bulkRequest, listener) -> esClient.bulkAsync(bulkRequest, DEFAULT, listener),
                new BulkLogger())
            .setBulkActions(5000)
            .setFlushInterval(TimeValue.timeValueSeconds(1))
            .setConcurrentRequests(0)
            .setBackoffPolicy(BackoffPolicy.exponentialBackoff(TimeValue.timeValueSeconds(2), 5))
            .build()) {
      try (InputStreamReader csv =
          new InputStreamReader(
              IngestMoviesMetadata.class.getClassLoader().getResourceAsStream(CSV_RESOURCE),
              StandardCharsets.UTF_8)) {
        Iterable<CSVRecord> records =
            CSVFormat.DEFAULT
                .withDelimiter(',')
                .withQuote('"')
                .withHeader(Header.class)
                .withFirstRecordAsHeader()
                .parse(csv);
        for (CSVRecord record : records) {
          readCount++;
          if (!record.isConsistent()) {
            skipCount++;
            continue;
          }
          LinkedHashMap<String, Object> doc = new LinkedHashMap<>(Header.values().length);
          String id = null;
          for (Header h : Header.values()) {
            final String rawValue = record.get(h);
            if (isEmpty(rawValue)) continue;
            Object docValue = rawValue;
            switch (h) {
              case adult:
              case video:
                docValue = Boolean.valueOf(rawValue);
                break;
              case id:
                id = rawValue;
                break;
              case belongs_to_collection:
              case genres:
              case production_companies:
              case production_countries:
              case spoken_languages:
                if (rawValue.startsWith("[")) {
                  docValue =
                      SUB_JSON_MAPPER.readValue(
                          cleanSubJson(rawValue),
                          new TypeReference<List<Map<String, Object>>>() {});
                } else {
                  docValue =
                      SUB_JSON_MAPPER.readValue(
                          cleanSubJson(rawValue), new TypeReference<Map<String, Object>>() {});
                }
                break;
            }

            doc.put(h.name(), docValue);
          }

          assertThat(id).isNotNull();
          bulkProcessor.add(new IndexRequest(index).id(id).source(doc));
        }
      }

      try {
        bulkProcessor.awaitClose(5, TimeUnit.MINUTES);
      } catch (final InterruptedException ignored) {
      }
    }

    esClient.indices().refresh(new RefreshRequest(index), DEFAULT);

    log.info("Read {} records, skipped {}", readCount, skipCount);
    log.info(
        SUB_JSON_MAPPER
            .writerWithDefaultPrettyPrinter()
            .writeValueAsString(
                SUB_JSON_MAPPER.readValue(
                    EntityUtils.toString(
                        esClient
                            .getLowLevelClient()
                            .performRequest(
                                new Request(
                                    "GET",
                                    "/_cat/indices/" + INDEX_BASE_NAME + "*?v&s=i&format=json"))
                            .getEntity()),
                    new TypeReference<List<Map<String, Object>>>() {})));
  }
}
